# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import eris

import std/[asyncdispatch, json, options, parseopt, streams]

proc usage() =
  stderr.writeLine """
Usage: erissum [OPTION]... [FILE]...
Print ERIS capabilities.

With no FILE, or when FILE is -, read standard input.

  --1k         1KiB block size
  --32k       32KiB block size (default)

  -t, --tag    BSD-style output
  -z, --zero   GNU-style output with zero-terminated lines
  -j, --json  JSON-style output

Default output format is GNU-style.
"""
  quit 0

proc fileCap(file: string, blockSize: Option[BlockSize]): ErisCap =
  var
    ingest: ErisIngest
    str: Stream
  if file == "-":
    str = newFileStream(stdin)
  else:
    try:
      str = newFileStream(file)
      doAssert(not str.isNil) # yes, that happens
    except:
      stderr.writeLine("failed to read \"", file, "\"")
      quit 1
  if blockSize.isSome:
    ingest = newErisIngest(newDiscardStore(), get blockSize, convergent = true)
  else:
    var
      buf = newSeq[byte](16 shl 10)
      p = addr buf[0]
    let n = readData(str, p, buf.len)
    if n == buf.len:
      ingest = newErisIngest(newDiscardStore(), bs32k, convergent = true)
    else:
      ingest = newErisIngest(newDiscardStore(), bs1k, convergent = true)
      assert n < buf.len
      buf.setLen n
    waitFor ingest.append(buf)
  waitFor ingest.append(str)
  close(str)
  waitFor ingest.cap

proc main*(opts: var OptParser) =
  var
    tagFormat, jsonFormat, zeroFormat: bool
    files = newSeq[string]()
    blockSize: Option[BlockSize]
  proc failParam(kind: CmdLineKind, key, val: string) =
    stderr.writeLine("unhandled parameter ", key, " ", val)
    quit 1

  for kind, key, val in getopt(opts):
    if val != "": failParam(kind, key, val)
    case kind
    of cmdLongOption:
      case key
      of "tag": tagFormat = true
      of "json": jsonFormat = true
      of "zero": zeroFormat = true
      of "1k": blockSize = some bs1k
      of "32k": blockSize = some bs32k
      of "help": usage()
      else: failParam(kind, key, val)
    of cmdShortOption:
      case key
      of "t": tagFormat = true
      of "j": jsonFormat = true
      of "z": zeroFormat = true
      of "": files.add("-")
      of "h": usage()
      else: failParam(kind, key, val)
    of cmdArgument: files.add(key)
    of cmdEnd: discard
  block:
    var flagged: int
    if tagFormat: inc(flagged)
    if jsonFormat: inc(flagged)
    if zeroFormat: inc(flagged)
    if flagged > 1:
      stderr.writeLine("refusing to output in multiple formats")
      quit -1
  if files == @[]:
    files.add("-")

  if jsonFormat:
    var js = newJArray()
    for i, file in files:
      let uri = $fileCap(file, blockSize)
      js.add(%*[file, uri])
    stdout.write($js)
  else:
    for i, file in files:
      let uri = $fileCap(file, blockSize)
      if tagFormat:
        stdout.writeLine("erisx2 (", file, ") = ", uri)
      elif zeroFormat:
        stdout.write(uri, "  ", file, '\0')
      else:
        stdout.writeLine(uri, "  ", file)

when isMainModule:
  var opts = initOptParser()
  main opts
