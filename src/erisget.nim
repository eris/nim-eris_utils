# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

# TODO: support for cbor:///…

import std/[asyncdispatch, parseopt, uri]
import eris, eris/url_stores

proc usage =
  stderr.writeLine """
Usage: erisget URL +URN
Get data from an ERIS store over the CoAP or HTTP protocol.

"""
  quit QuitFailure

proc die(args: varargs[string, `$`]) =
  writeLine(stderr, args)
  if not defined(release): raiseAssert "die"
  quit QuitFailure

proc get(store: ErisStore; cap: ErisCap) =
  var
    stream = newErisStream(store, cap)
    buf = newString(int cap.blockSize)
  while buf.len == int cap.blockSize:
    let n = waitFor stream.readBuffer(buf[0].addr, buf.len)
    if n < buf.len: buf.setLen n
    stdout.write buf
  close(stream)

proc failParam(kind: CmdLineKind, key, val: string) =
  die "invalid parameter ", kind, " \"", key, ":", val, "\""

proc main*(opts: var OptParser) =
  var
    store: ErisStore
    caps: seq[ErisCap]

  for kind, key, val in getopt(opts):
    case kind
    of cmdLongOption:
      if val != "": failParam(kind, key, val)
      case key
      of "help": usage()
      else: failParam(kind, key, val)
    of cmdShortOption:
      case key
      of "h": usage()
      else: failParam(kind, key, val)
    of cmdArgument:
      assert key != ""
      if store.isNil:
        try:
          var url = parseUri(key)
          store = waitFor newStoreClient(url)
        except:
          die "failed to connect to ", key
      else:
        try:
          caps.add key.parseErisUrn
        except:
          die "failed to parse ", key, " as an ERIS URN"
    of cmdEnd: discard
  if store.isNil: die "no store URL specified"
  for cap in caps: get(store, cap)

when isMainModule:
  var opts = initOptParser()
  main opts
