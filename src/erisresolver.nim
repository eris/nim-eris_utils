# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

# TODO: timeouts!

import std/[asyncdispatch, asyncfutures, monotimes, net, parseopt, tables, times, uri]
from std/sequtils import toSeq
import preserves, syndicate, syndicate/actors
import coap/common
import ./erisresolver_config
import eris, eris/[coap_stores, http_stores, syndicate_stores, tkrzw_stores, url_stores]

type
  Uri = uri.Uri
  CoapUrl = common.Uri

type MeasuredStore = ref object of ErisStoreObj
  store: ErisStore
  sum, count: float
  ops: Operations

method get(s: MeasuredStore; blkRef: Reference; bs: BlockSize; futGet: FutureGet) =
  assert Get in s.ops
  let
    a = getMonoTime()
    interFut = newFutureGet(bs)
  get(s.store, blkRef, bs, interFut)
  interFut.addCallback do (interFut: FutureGet):
    let b = getMonoTime()
    s.sum = s.sum + (b - a).inMilliseconds.float
    s.count = s.count + 1
    if interFut.failed:
      fail(futGet, interFut.readError)
    else:
      copyBlock(futGet, bs, interFut.mget)
      complete(futGet)

type
  MultiStore = ref object of ErisStoreObj
    stores: OrderedTable[Uri, MeasuredStore]

proc sortStores(multi: MultiStore) =
  ## Sort the stores in a `MultiStore` by average response time.
  func averageRequestTime(store: MeasuredStore): float =
    store.sum / store.count
  func cmpAverage(x, y: (Uri, MeasuredStore)): int =
    # TODO: weighted average
    int y[1].averageRequestTime - x[1].averageRequestTime
  sort(multi.stores, cmpAverage)

method get(multi: MultiStore; r: Reference; bs: BlockSize; futGet: FutureGet) =
  let
    keys = multi.stores.keys.toSeq
    interFut = newFutureGet(bs)
  proc getFromStore(storeIndex: int) =
    if storeIndex > keys.high:
      sortStores(multi)
      fail(futGet, interFut.readError)
    else:
      clean(interFut)
      get(multi.stores[keys[storeIndex]], r, bs, interFut)
      interFut.addCallback do (interFut: FutureGet):
        if interFut.failed: getFromStore(succ storeIndex)
        else:
          if storeIndex > 0: sortStores(multi)
          copyBlock(futGet, bs, interFut.mget)
          complete(futGet)
  if keys.len == 0: fail(futGet, newException(IOError, "no stores to query"))
  else: getFromStore(keys.low)

method put(s: MultiStore; r: Reference; parent: PutFuture) =
  var pendingFutures, completedFutures, failures: int
  assert s.stores.len > 0
  for key, measured in s.stores:
    if Put in measured.ops:
      var child = newFutureVar[seq[byte]]("MultiStore")
      (child.mget) = parent.mget
      cast[Future[seq[byte]]](child).addCallback do (child: Future[seq[byte]]):
        if child.failed: inc failures
        inc completedFutures
        if completedFutures == pendingFutures:
          if failures > 0:
            fail(cast[Future[seq[byte]]](parent), newException(IOError, "put failed for some stores"))
          else:
            complete(parent)
      inc pendingFutures
      measured.store.put(r, child)
  if pendingFutures == 0:
    fail(cast[Future[seq[byte]]](parent),
      newException(IOError, "no stores to put to"))

proc main*(opt: var OptParser) =
  if opt.kind != cmdEnd:
    quit "invalid parameter " & opt.key

  bootDataspace("main") do (ds: Ref; turn: var Turn):
    var resolver = MultiStore()
    stderr.writeLine "Connecting to Syndicate peer over stdio…"
    connectStdio(ds, turn)
    stderr.writeLine "Connected."

    during(turn, ds, ?TkrzwDatabase) do (path: string, ops: Operations):
      let
        uri = Uri(scheme: "tkrzw", path: path)
        dbStore = newDbmStore(path, ops)
      resolver.stores[uri] = MeasuredStore(store: dbStore, ops: ops)
      stderr.writeLine("opened store at ", uri)
    do:
      if resolver.stores.hasKey uri:
        close(resolver.stores[uri])
        resolver.stores.del(uri)
      stderr.writeLine("closed store at ", uri)

    during(turn, ds, ?Peer) do (s: string; ops: Operations):
      let uri = parseUri(s)
      if not resolver.stores.hasKey uri:
        url_stores.newStoreClient(uri).addCallback do (fut: Future[ErisStore]):
          resolver.stores[uri] = MeasuredStore(store: fut.read, ops: ops)
          stderr.writeLine("opened store at ", uri)
    do:
      if resolver.stores.hasKey uri:
        close(resolver.stores[uri])
        resolver.stores.del(uri)
      stderr.writeLine("closed store at ", uri)

    during(turn, ds, ?CoapServer) do (ip: string; port: Port; ops: Operations):
      var server = coap_stores.newServer(resolver, ops)
      server.serve(ip.parseIpAddress, port)
      stderr.writeLine("serving CoAP sessions on ", ip)
    do:
      try: close(server)
      except: discard
      stderr.writeLine("stopped listening for CoAP sessions on ", ip)

    during(turn, ds, ?HttpServer) do (ip: string; port: Port; ops: Operations):
      var server = http_stores.newServer(resolver)
      asyncCheck(turn, server.serve(ops, ip.parseIpAddress, port))
      stderr.writeLine("serving HTTP sessions on ", ip)
    do:
      try: close(server)
      except: discard
      stderr.writeLine("stopped listening for HTTP sessions on ", ip)

    during(turn, ds, ?SyndicateRelay[Ref]) do(ds: Ref, ops: Operations):
      stderr.writeLine "Starting relay to ", ds, '.'
      let storeFacet = newStoreFacet(turn, resolver, ds, ops)
    do:
      stop(turn, storeFacet)
      stderr.writeLine "Stopped relay to ", ds, '.'

  runForever()

when isMainModule:
  var opts = initOptParser()
  main opts
