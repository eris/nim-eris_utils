# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, options, parseopt, streams]
import eris, eris/cbor_stores

proc usage() =
  stderr.writeLine """
Usage: eriscbor [OPTION]... FILE [URI]...
Encode or decode CBOR serialized ERIS blocks.

When URIs are supplied then data is read from FILE to stdout,
otherwise data from stdin is written to FILE and a URN is
written to stdout.

Option flags:
	--1k           1KiB block size
	--32k         32KiB block size
	--convergent  generate convergent URNs (unique by default)
	--with-caps   include read-capabilities in FILE

"""
  quit QuitFailure

proc die(args: varargs[string, `$`]) =
  writeLine(stderr, args)
  if not defined(release): raiseAssert "die"
  quit QuitFailure

proc failParam(kind: CmdLineKind, key, val: string) =
  die "unhandled parameter ", key, " ", val

proc main*(opts: var OptParser) =
  var
    cborFilePath = ""
    blockSize: Option[BlockSize]
    caps: seq[ErisCap]
    convergent, withCaps: bool

  for kind, key, val in getopt(opts):
    case kind
    of cmdLongOption:
      if val != "": failParam(kind, key, val)
      case key
      of "1k": blockSize = some bs1k
      of "32k": blockSize = some bs32k
      of "convergent": convergent = true
      of "with-caps": withCaps = true
      of "help": usage()
      else: failParam(kind, key, val)
    of cmdShortOption:
      if val != "": failParam(kind, key, val)
      case key
      of "h", "?": usage()
      else: failParam(kind, key, val)
    of cmdArgument:
      if cborFilePath == "": cborFilePath = key
      else:
        try: caps.add(parseErisUrn key)
        except:
          die "failed to parse ERIS URN ", key
    of cmdEnd: discard

  if cborFilePath == "":
    die "A file must be specified"

  let encode = caps.len == 0
  if encode:
    stderr.writeLine "encoding from stdin"
    var
      fileStream = openFileStream(cborFilePath, fmAppend)
      store = newCborEncoder(fileStream)
      cap =
        if blockSize.isSome:
          waitFor encode(store, blockSize.get, newFileStream(stdin), convergent)
        else:
          waitFor encode(store, newFileStream(stdin), convergent)
    if withCaps:
      store.add(cap)
    stdout.writeLine cap
    close(store)
    close(fileStream)

  else:
    stderr.writeLine "decoding to stdout"
    var
      fileStream = openFileStream(cborFilePath, fmRead)
      store = newCborDecoder(fileStream)
    for cap in caps:
      let erisStream = newErisStream(store, cap)
      waitFor dump(erisStream, newFileStream(stdout))
      close(store)

when isMainModule:
  var opts = initOptParser()
  main opts
