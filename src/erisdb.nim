# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, os, parseopt, streams, strutils]
import tkrzw
import eris, eris/tkrzw_stores

const
  dbEnvVar = "eris_db_file"
  smallBlockFlag = "1k"
  bigBlockFlag = "32k"
  usageMsg = """
Usage: erisdb [OPTION]... [URI]...
Read and write ERIS encoded content to a file-backed database.

The location of the database file is configured by the "$1"
environment variable.

Each URI specified is written to stdout. If no URIs are specified then
read standard input into the database and print the corresponding URI.

Option flags:
	--$2 	 1KiB block size
	--$3	32KiB block size (default)

"""  % @[dbEnvVar, smallBlockFlag, bigBlockFlag ]

proc usage =
  stderr.writeLine usageMsg
  quit QuitFailure

proc output(store: ErisStore; cap: ErisCap) =
  var
    buf: array[32 shl 10, byte]
    bp = addr buf[0]
  try:
    var str = store.newErisStream(cap)
    while not str.atEnd:
      let n = waitFor str.readBuffer(bp, buf.len)
      var off = 0
      while off < n:
        let N = stdout.writeBytes(buf, off, n)
        if N == 0: quit "closed pipe"
        off.inc N
  except:
    stderr.writeLine getCurrentExceptionMsg()
    quit "failed to read ERIS stream"

proc input(store: ErisStore; blockSize: BlockSize): ErisCap =
  try:
    result = waitFor encode(store, blockSize, newFileStream(stdin))
  except:
    stderr.writeLine getCurrentExceptionMsg()
    quit "failed to ingest ERIS stream"

proc main*(opts: var OptParser) =
  var
    erisDbFile = getEnv(dbEnvVar, "eris.tkh")
    outputUris: seq[string]
    blockSize = bs32k

  proc failParam(kind: CmdLineKind, key, val: string) =
    quit "unhandled parameter " & key & " " & val

  for kind, key, val in getopt(opts):
    case kind
    of cmdLongOption:
      case key
      of smallBlockFlag: blockSize = bs1k
      of bigBlockFlag: blockSize = bs32k
      of "help": usage()
      else: failParam(kind, key, val)
    of cmdShortOption:
      case key
      of "h": usage()
      else: failParam(kind, key, val)
    of cmdArgument:
      outputUris.add key
    of cmdEnd: discard

  if outputUris == @[]:
    var store = newDbmStore(erisDbFile, {Put})
    let cap = input(store, blockSize)
    stdout.writeLine($cap)
    if store.dbm.shouldBeRebuilt:
      stderr.writeLine("rebuilding ", erisDbFile, "…")
      rebuild store.dbm
    close store
  else:
    var store = newDbmStore(erisDbFile, {Get})
    for uri in outputUris:
      try:
        let cap = parseErisUrn uri
        output(store, cap)
      except ValueError:
        stderr.writeLine "failed to parse ", uri
        quit getCurrentExceptionMsg()

when isMainModule:
  var opts = initOptParser()
  main opts
