# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[os, parseopt, strutils]
import ./eriscbor, ./erisdb, ./erisdbmerge, ./erisget, ./erisinfo, ./erispad, ./erisput, ./erisresolver, ./erissum

proc completionsFish(opts: var OptParser) {.gcsafe.}

const commands = [
  ("cbor", eriscbor.main),
  ("completions.fish", completionsFish),
  ("db", erisdb.main),
  ("dbmerge", erisdbmerge.main),
  ("get", erisget.main),
  ("info", erisinfo.main),
  ("pad", erispad.main),
  ("put", erisput.main),
  ("resolver", erisresolver.main),
  ("sum", erissum.main),
]

proc completionsFish(opts: var OptParser) =
  stdout.write "complete --command ", getAppFilename().extractFilename, " --arguments ' "
  for cmd in commands:
    stdout.write cmd[0], " "
  stdout.writeLine "'"

if paramCount() < 1:
  stderr.writeLine "Subcommands"
  for cmd in commands: stderr.writeLine "\t", cmd[0]
  quit "Subcommand required."

var programName = getAppFilename().extractFilename.normalize
let isCalledAsEriscmd = programName == "eriscmd" or programName == "eris"
if isCalledAsEriscmd:
  programName = paramStr(1).normalize

proc call(entrypoint: proc (opts: var OptParser)) =
  var
    opts: OptParser
    args = commandLineParams()
  if isCalledAsEriscmd:
    args = args[1..args.high]
  if args.len > 0:
    opts = initOptParser(args)
  entrypoint(opts)

for cmd in commands:
  if programName == cmd[0] or programName == ("eris" & cmd[0]):
    call cmd[1]
    quit QuitSuccess

quit("unhandled command \"$#\"" % programName)
