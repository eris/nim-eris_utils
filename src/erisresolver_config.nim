# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import
  std/typetraits, preserves
from eris import Operations

type
  HttpServer* {.preservesRecord: "http-server".} = object
    `ip`*: string
    `port`*: int
    `ops`*: Operations

  Peer* {.preservesRecord: "peer".} = object
    `uri`*: string
    `ops`*: Operations

  SyndicateRelay*[E] {.preservesRecord: "syndicate".} = ref object
    `dataspace`*: Preserve[E]
    `ops`*: Operations

  TkrzwDatabase* {.preservesRecord: "tkrzw".} = object
    `path`*: string
    `ops`*: Operations

  CoapServer* {.preservesRecord: "coap-server".} = object
    `ip`*: string
    `port`*: int
    `ops`*: Operations

proc `$`*[E](x: SyndicateRelay[E]): string =
  `$`(toPreserve(x, E))

proc encode*[E](x: SyndicateRelay[E]): seq[byte] =
  encode(toPreserve(x, E))

proc `$`*(x: HttpServer | Peer | TkrzwDatabase | CoapServer | Operations): string =
  `$`(toPreserve(x))

proc encode*(x: HttpServer | Peer | TkrzwDatabase | CoapServer | Operations): seq[byte] =
  encode(toPreserve(x))
